
    db.events.aggregate([...])

---

#### Production and storing

    EventDispatcher -> Decoratori -> Job in RabbitMQ -> Mongo

---

#### Mongo collection and documents

Tutti gli eventi sono salvati in una sola collection

La struttura base dei documenti:

	{
        "_id" : ObjectId("5898ab5a22c92d69123f7281"),
        "type" : "agendaCreated",
        "eventFamily" : "agenda",
        "meta" : {
            
            // Instate in cui è stato generato
            "createdAt" : ISODate("2017-02-06T17:59:05"),
            
            // Instante in cui è stato salvato su mongo
            "receivedAt" : ISODate("2017-02-06T17:59:06"),
            "currentUserId" : 29877,
            "source" : "shark",
            "context" : "web",
            "taskId" : 3503425,
            "productId" : 5937725,
            ... 
        },
        "payload" : {
            "currentUser" : {
                "id" : 29877,
                ...
            },
            "task" : {
                "id" : 3503425,
                ...
            },
            "product" : {
                "id" : 5937725,
                ...
            }
            ...
        }
	}

---

#### Immediate advantages

- Completo log di tutti i cambiamenti di stato
- Debugability and traceability
- Easy way to undo things

---

#### Our Main goal: reporting

- Complesse query per reportistica/bonus system 
- Versionamento delle query per bonus system
- Mancanza di alcuni dati storicizzati su mysql

---

*Problema:*

Collection eventi ~ 4GB dopo 6 mesi, praticamente impossibile fare query

*Soluzione:* 

Proiettori, derivare lo stato corrente di un oggetto percorrendo tutto lo stream di eventi 
salvando il risultato in modelli (semplici contatori oppure oggetti complessi)

---

##### Example 

*Contare il numero di prodotti emessi per mese da un account*

	{
		"_id" : ObjectId("5898ab5a22c92d69123f7281"),
		"type" : "productIssued",
		"meta" : {
	        "createdAt" : ISODate("2017-02-06T07:00:00.000"),
	        "productId" : 9,
	        "accountId" : 1,
	    },
	    "payload" : {
	    	"product" : {
	    		"id" : 9,
	    		"carPlate" : "AA000AA"
	    	}
	    }
	}
	{
		"_id" : ObjectId("5898ab5a22c92d69123f7282"),
		"type" : "productIssued",     
		"meta" : {
	        "createdAt" : ISODate("2017-02-06T08:00:00.000"),
	        "productId" : 10,
	        "accountId" : 2,
	        "payload" : {
	    		"product" : {
	    			"id" : 10,
	    			"carPlate" : "AA001AA"
	    		}
	    	}
	    }
	}

---

Results model 1:

	{
		"_id" : ObjectId("5898ab5a22c92d69123f7283"),
		"accountId": 1,
		"yearMonth" : "2017-02",
		"productsIssuedCount" : 1
	}
	{
		"_id" : ObjectId("5898ab5a22c92d69123f7284"),
		"accountId": 2,
		"yearMonth" : "2017-02",
		"productsIssuedCount" : 1
	}

---

Results model 2:

	{
		"_id" : ObjectId("5898ab5a22c92d69123f7285"),
		"accountId": 1,
		"yearMonth" : "2017-02",
		"productsIssued" : [
			{
	    		"id" : 9
	    		"carPlate" : "AA000AA"
	    	}
		]
	}
	{
		"_id" : ObjectId("5898ab5a22c92d69123f7286"),
		"accountId": 2,
		"yearMonth" : "2017-02",
		"productsIssued" : [
			{
    			"id" : 10
    			"carPlate" : "AA001AA"
	    	}
		]
	}

---

#### Our implementation

- ProjectionsRunner + RunState
- Projector + Executor
- ProjectionState

---

#### ProjectionsRunner + RunState

- Un ProjectionsRunner per tutti i proiettori, un RunState per ogni proiettore
- Lo scopo principale del runner eseguire query sulla collection events e passarne uno alla volta al proiettore

---

class ProjectionsRunner

    class ProjectionsRunner
    {
        public function run(ProjectorInterface $projection);

        private function loadRunState(ProjectorInterface $projection): RunState

        private function cleanAndUpdateRunState(ProjectorInterface $projection, RunState $runState);
    }

---

Il problema principale del runner: Ordinare gli eventi

*Esempio: contare il numero di polizze emesse quando il collaboratore è in stato disdettato*

    {
        "_id" : ObjectId("000000000000000000000001"),
        "type" : "productIssued",
        "meta" : {
            "createdAt" : ISODate("2017-03-07T08:00:00"),
            "receivedAt" : ISODate("2017-03-07T08:00:00"),
            "productId" : 10,
            "collaboratoreId": 1
        }
    }
    {
        "_id" : ObjectId("000000000000000000000002"),
        "type" : "collaboratoreStatusChanged",
        "meta" : {
            "createdAt" : ISODate("2017-02-06T08:00:00"),
            "receivedAt" : ISODate("2017-03-08T08:00:00"),
            "status" : "disdettato",
            "collaboratoreId": 1,
        }
    }

---

RunState:

	{
	    "_id" : ObjectId("5899027b78d56d025726df62"),
	    "class" : "EventSourcingBundle\\ReteProductionProjector",
	    "lastId" : ObjectId("000000000000000000000001"),
	    "lastProjectedCreateAt" : ISODate("1969-12-31T00:00:00"),
	    "running" : false,
	    "lastRunningAt" : ISODate("2017-02-06T00:10:51")
	}

- *lastId*: max id processato (potrebbe non essere l'ultimo) 
- *lastProjectedCreateAt*: ultimo CreateAt processato
- *running*: true se fallito iterazione precedente
- *lastRunningAt*: per mostrare aggiornamento dati

---

All'iterazione successiva il runner deve sempre basarsi sul *lastId*,
non può usare *lastProjectedCreateAt*

    db.getCollection('events').find({
        "_id" : {"$gt" => ObjectId("000000000000000000000001")},
        "type" : {"$in" => ['type1', 'type3']},
    }).sort({'meta.createdAt' : 1})

---

#### interface ProjectorInterface

	interface ProjectorInterface
	{
	    /**
	     * @return bool
	     */
	    public function needEventOrderedByCreatedAt(): bool;

	    /**
	     * @return array
	     */
	    public function getEventTypes(): array;

	    /**
	     * @param Event $event
	     *
	     * @return ProjectorExecutorResult
	     */
	    public function projectAndSave(Event $event): ProjectorExecutorResult;

	    /**
	     * @param \DateTime $dateTime
	     */
	    public function cleanAfterDate(\DateTime $dateTime): \DateTime;

	    /**
	     */
	    public function initializeProjector();

	    /**
	     * @param \DateTime $firstCreatedAt
	     * @param \DateTime $lastCreatedAt
	     */
	    public function normalizeAfterRun(\DateTime $firstCreatedAt, \DateTime $lastCreatedAt);
	}

---

#### interface ProjectorExecutorInterface

	interface ProjectorExecutorInterface
	{
	    /**
	     * @return array
	     */
	    public function supportEventsType():array;

	    /**
	     * @param Event $event
	     * @return ProjectorExecutorResult
	     */
	    public function execute(Event $event)
	                                   : ProjectorExecutorResult;
	}

---

#### class ProjectorExecutorResult

	class ProjectorExecutorResult
	{ 
		...

		/**
	     * @return ProjectorExecutorResult
	     */
	    public static function success()
	    {
	        return new self(true);
	    }

	    /**
	     * @param string $message
	     * @param array $msgArguments
	     *
	     * @return ProjectorExecutorResult
	     */
	    public static function fail(string $message, array $msgArguments = [])
	    {
	        return new self(false, $message, $msgArguments);
	    }

	    ...
	}

---

#### ProjectionState

Problemi: 
- Proiettori non sono idempotenti f(x) = f(f(x))
    (se si passa ad un Executor due volte lo stesso evento: productIssued? )
- Mancanza di informazioni nell'evento corrente

---

#### Example - Contare le polizze emesse per mese

	{
		"_id" : ObjectId("5898ab5a22c92d69123f7284"),
		"type" : "productIssued",
		"meta" : {
	        "createdAt" : ISODate("2017-02-06T08:00:00"),
	        "issuedAt" : ISODate("2017-02-07T08:00:00"),
	        "productId" : 10
	    }
	}
	{
		"_id" : ObjectId("5898ab5a22c92d69123f7281"),
		"type" : "productRefund",
		"meta" : {
	        "createdAt" : ISODate("2017-02-07T08:00:00"),
	        "issuedAt" : null,
	        "productId" : 10
	    }
	}

---

ProjectionState

	{
	    "_id" : ObjectId("589908c878d56d02a760b8e4"),
	    "class" : "EventSourcingBundle\\ReteProductionProjector",
	    "correlationId" : "productId_10",
	    "status" : {
	        "issuedYearMonth" : "2017-02"
	    },
	    "expireAt": ISODate("2018-02-07T08:00:00"),
	}

---

Expire data from collections by setting TTL

	db.projection_state.createIndex( 
	    { "expireAt": 1 }, 
	    { expireAfterSeconds: 0 } 
	)

---

#### Ricostruire gli eventi

Tabella task in mysql 

    [id, taskType, userId, createdAt, updateAt]

Vogliamo ricostruire gli eventi taskCreated, 

sappiamo che userId potrebbe cambiare nel tempo

---

Il createdAt dell'evento sarà il createdAt di mysql oppure l'istante in cui viene ricostruito?

    {
        "_id" : ObjectId("5898ab5a22c92d69123f7281"),
        "type" : "taskCreated",
        "meta" : {
            "createdAt" : ISODate("2017-02-07T08:00:00"),
            "userId" : 10
        }
    }

---

Considerando che *userId* potrebbe cambiare,

prendendo l'istante in cui viene ricostruito,

il nostro evento ci comunica il momento in cui quell'informazione è vera nel nostro sistema,

cosa che potrebbe non essere vera considerando il *createdAt* di mysql

---

Però rimane una differenza sostanziale:

1 - Nel primo caso potremmo dire una cosa falsa (*userId*)

2 - Nel secondo caso stiamo dicendo sicuramente una cosa falsa (*createdAt*)

---

Noi abbiamo scelto la prima strategia

---

#### Come scrivere test funzionali con paraunit ?

    public function dataProvider()
    {
        $data = [];

        $data[] = [
            [
                EventStubs::getEventIssuedPolizza(),
                EventStubs::getEventRinnovoDiaryCreated(),
                EventStubs::getEventIssuedRinnovo()
            ],
            Expectations::issuedProductIssuedProduction()
        ];

        ...
    }
    
Infiniti problemi di concorrenza ...

---

al setUp()

    public function mongoUseCloneDb()
    {
        if (null !== $this->mongoDbClonedName) {
            throw new \LogicException('Call only one time, please');
        }

        $currentName = $this->getContainer()->getParameter('mongo_collaboratori_database');
        $newDbName = $currentName.'_'.uniqid();

        $client = $this->getContainer()->get('mongo.client_registry')->getClient('shark_client_test');
            
        $client->selectDatabase('admin')->command([
            'copydb' => 1,
            'fromdb'=> $currentName.'_test',
            'todb'=> $newDbName,
        ]);

        $this->getContainer()
            ->set('mongo.connection.'.$currentName, $client->selectDatabase($newDbName));

        $this->mongoDbClonedName = $newDbName;
    }

---

al tearDown()

    public function mongoDropCloneDb()
    {
        if (null === $this->mongoDbClonedName) {
            return ;
        }

        $client = $this->getContainer()->get('mongo.client_registry')->getClient('shark_client_test');
        $client->selectDatabase($this->mongoDbClonedName)->drop();
        $this->mongoDbClonedName = null;
    }

---

fine