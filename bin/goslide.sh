#!/bin/bash

if [ $0 != 'bin/goslide.sh' ]; then
    echo ''
    echo '      Start from project root: bin/goslide.sh'
    echo ''
    exit 1
fi

WORKDIR=$(pwd)

docker run \
    --rm \
    -i \
    -t \
    -u iacopo \
    -v $WORKDIR:/slide \
    -w /slide \
    -p 1948:1948 \
    iacopo/node-bower-grunt \
    ./node_modules/.bin/reveal-md slides.md